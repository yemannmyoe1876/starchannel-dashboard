@extends('layouts.app')
  
@section('title', 'Show movies')
  
@section('contents')
    <h1 class="mb-0">Detail Movies</h1>

    <hr />
    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Movies Name</label>
            <input type="text" name="movies_name" class="form-control" value="{{ $movies->name }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Url Name</label>
            <input type="text" class="form-control" value="{{ $movies->url_name }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Categories</label>
            <input type="text" name="category" class="form-control" value="{{ $movies->categories }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Genres</label>
            <input type="text" class="form-control" value="{{ $movies->genres }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Director</label>
            <input type="text" class="form-control" value="{{ $movies->director }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Actors</label>
            <input type="text" class="form-control" value="{{ $movies->actors }}" readonly>
        </div>
       
    </div>

 

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Producer</label>
            <input type="text" class="form-control" value="{{ $movies->producer }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Production Date</label>
            <input type="text" class="form-control" value="{{ date('m-d-Y', strtotime($movies['production_date'])) }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Posted Date</label>
            <input type="text" class="form-control" value="{{date('m-d-Y', strtotime ( $movies['posted_date'])) }}" readonly>
        </div>
       
    </div>


    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Description</label>
            <input type="text" class="form-control" value="{{ $movies->description }}" readonly>
        </div>
       
    </div>


  
    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Has Season</label>
            <input type="text" class="form-control" value="{{ $movies->has_season }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Season Id</label>
            <input type="text" class="form-control" value="{{ $movies->season_id }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Preview</label>
            <input type="text" class="form-control" value="{{ $movies->thumbnail_image }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">MyDay</label>
            <input type="read" class="form-control" value="{{ $movies->images }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Vimeo Url</label>
            <input type="text" class="form-control" value="{{ $movies->vimeo_url }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Youtube Url</label>
            <input type="text" class="form-control" value="{{ $movies->youtube_url }}" readonly>
        </div>
       
    </div>

    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="option">Channel</label>
            <input type="text" class="form-control" value="{{ $movies->channel }}" readonly>
              

              {{-- @foreach($channel as $channel)
              <select name="movies" class="form-control" value="{{$channel->channel_name}}">{{$channel->channel_name}}</select>
              @endforeach --}}
            </div>
       
    </div>
  
    <div class="row mb-3">
       
        <div class="col-md-6">
            <label for="movies_name">Visible</label>
            <input type="text" class="form-control" value="{{ $movies->visible }}" readonly>
        </div>
       
    </div>
  
  
  
        <div class="row mb-3">
            <div class="col-md-6">
            <label class="form-label">Created At</label>
            <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $movies->created_at }}" readonly>
        </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-6">
            <label class="form-label">Updated At</label>
            <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $movies->updated_at }}" readonly>
        </div>
        </div>
   
@endsection