@extends('layouts.app')
  
@section('title', 'Home movies')
  
@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Movies List</h1>
        <a href="{{ route('movies.create') }}" class="btn btn-primary">Add Movies</a>
    </div>
    <hr />

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif

    <table class="table table-hover">
        <thead class="table-primary">
            <tr>
                <th>#</th>
                <th>Movies_name</th>
                <th>Channel_Name</th>
                <th>Production_date</th>
                <th>Vimeno_Url</th>
                <th>Visible</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>+
            @if($movies->count() > 0)
                @foreach($movies as $array)
                    <tr>
                        <td class="align-middle">{{ $array->iteration }}</td>
                        <td class="align-middle">{{ $array->name }}</td>
                        <td class="align-middle">{{ $array->channel }}</td>
                        <td>{{ date('m-d-Y', strtotime($array['production_date'])) }}</td>
                    
                        <td class="align-middle">{{ $array->vimeo_url }}</td>
                        <td class="align-middle">
                            @if ($array['visible'] == 1)
                        <p class="text-success">Yes</p>
                    @else
                    <p class="text-danger">No</p>
                    @endif    
                        </td>  
                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                 <a href="{{route('movies.show', $array->id)}}" type="button" class="btn btn-info">Detail</a>
                                <a href="{{route('movies.edit', $array->id)}}" type="button" class="btn btn-warning">Edit</a>
                                <form action="{{ route('movies.destroy', $array->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')


                                <button class="btn btn-danger m-0">Delete</button>
                              
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">data not found</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection