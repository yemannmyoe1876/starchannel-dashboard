@extends('layouts.app')

@section('contents')

<div class="container mt-5">
    <div class="col-md-12">
        <h1>Channel</h1>
        <form action="{{ route('movies.update', $movies->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            

            <div class="row mb-3">
       
                <div class="col-md-6">
                    <label for="movies_name">Movies Name</label>
                    <input type="text" name="movies_name" class="form-control" value="{{ $movies->name }}">
                </div>
               
            </div>

            
            <div class="row mb-3">
       
                <div class="col-md-6">
                    <label for="url_name">Url Name</label>
                    <input type="text" name="url_name" class="form-control" value="{{ $movies->url_name }}">
                </div>
               
            </div>

            <div class="row mb-3">
       
                <div class="col-md-6">
                    <label for="movies_name">Categories</label>
                    <select name="category" id="category" class="form-control">
                        @foreach($category as $category)
                        <option value="{{ $category->category_name }}" {{$movies->id == $movies->id ? 'selected' : ''}}>{{ $category->category_name }}</option>
                        @endforeach
                      </select>
                </div>
               
            </div>
                                                                        

            
            <div class="row mb-3">
       
                <div class="col-md-6">
                    <label for="movies_name">Genres</label>
                    <select name="genres" id="genres" class="form-control">
                        @foreach($generes as $generes)
                        <option value="{{ $generes->generes_name }}">{{ $generes->generes_name }}</option>
                        @endforeach
                      </select>
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Director</label>
                    <input type="text"  name="director" class="form-control" value="{{ $movies->director }}" >
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Actors</label>
                    <input type="text"  name="actors"  class="form-control" value="{{ $movies->actors }}" >
                </div>
               
            </div>
    
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Producer</label>
                    <input type="text" name="producer" class="form-control" value="{{ $movies->producer }}" >
                </div>
               
            </div>
        
            {{-- <div class="row mb-3">
                <div class="col-md-6">
                    <label for="production_date">Production Date</label>               
                    <input type="date" class="form-control @error('production_date') is-invalid @enderror" id="production_date" value="{{$ }}">
                    @error('production_date')
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                </div>
               
            </div>
             --}}
         

                
        <div class="row mb-3">
            <div class="col-md-6">
                @if ($movies->production_date !=null)
                <label for="posted_date">Production Date</label>
                <input type="date" class="form-control" id="production_date" name="production_date" value="{{$format_prod_date}}">
                @endif
            </div>
          
        </div>




        <div class="row mb-3">
            <div class="col-md-6">
                @if ($movies->posted_date !=null)
                <label for="posted_date">Posted Date</label>
                <input type="date" class="form-control" id="posted_date" name="posted_date" value="{{$format_post_date}}">
                @endif
            </div>
          
        </div>
 



        
                   @push('scripts')
   <script type="text/javascript">
           $('#production_date').datetimepicker({
               format: 'YYYY-MM-DD',
               sideBySide: true
           })
       </script>
@endpush

@push('scripts')
   <script type="text/javascript">
           $('#posted_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush

        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Description</label>
                   
                   
                    <textarea type="text" name="description" class="form-control" value="{{ $movies->description }}" >{{$movies->description}}</textarea>
                </div>
               
            </div>
       
          
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Has Season</label>
                    <input type="text" name="has_season" class="form-control" value="{{ $movies->has_season }}" >
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    @if ($movies->season_id !=null)
                    <label for="movies_name">Season Id</label>
                    <select name="season_id" id="season_id" class="form-control">
                        @foreach($season as $season)
                                <option value="{{ $season->id }}">{{ $season->season_name }}</option>
                         @endforeach
                    </select>
                    @endif
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Preview</label>
                    <input type="file" name="thumbnail_image"  class="form-control" value="{{ $movies->thumbnail_image }}" >
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">MyDay</label>
                    <input type="file" name="images" class="form-control" value="{{ $movies->images }}" >

                    {{-- <img src="{{asset($movies->images)}}" width="200px"> --}}
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Vimeo Url</label>
                    <input type="text" name="vimeo_url" class="form-control" value="{{ $movies->vimeo_url }}" >
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Youtube Url</label>
                    <input type="text" name="youtube_url" class="form-control" value="{{ $movies->youtube_url }}" >
                </div>
               
            </div>
        
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Channel</label>
                    {{-- <input type="text" name="option" class="form-control" value="{{ $movies->channel }}" > --}}

                    <select name="option" id="option" class="form-control">
                        @foreach($channel as $channel)
                        <option value="{{ $channel->channel_name }}">{{ $channel->channel_name }}</option>
                        @endforeach
                    </select>
                    
                  
{{-- 
                      @foreach($channel as $channel)
                      <option value="{{$channel->id}}" 
                        @if($movies->channel == $channel->channel_name){{'selected'}}@endif>{{$channel->channel_name}}
                      </option>
                      @endforeach --}}
                      
                </div>
               
            </div>

           
          
            <div class="row mb-3">
               
                <div class="col-md-6">
                    <label for="movies_name">Visible</label>
                    {{-- <input type="text" name="visible" class="form-control" value="{{ $movies->visible }}" > --}}

                    <select class="form-control" id="visible" name="visible">
                        <option value="1">yes</option>
                        <option value="0">no</option>
                    </select>
                </div>
               
            </div>
          
          
          
                <div class="row mb-3">
                    <div class="col-md-6">
                    <label class="form-label">Created At</label>
                    <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $movies->created_at }}" readonly>
                </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                    <label class="form-label">Updated At</label>
                    <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $movies->updated_at }}" readonly>
                </div>
                </div>
           
            <button type="submit" class="btn btn-success float-right">Update</button>
        </form>
    </div>
</div>

@section('scripts')
    <script>
        $(function(){
            $('.channel_logo_update').on('change',function(){
                var channel_url = window.URL.createObjectURL(this.files[0]);
                $('.channel_view').html('<img id="channel_preview" src="'+ channel_url +'" class="mt-3 text-center" alt="" width="400" height="400">');
            });
        })
    </script>
@endsection
@endsection