@extends('layouts.app')
  
@section('title', 'Create movies')
  
@section('contents')
  
    <hr />
    <form action="{{route('movies.store')}}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row mb-3">
       
            <div class="col-md-6">
                <label for="">Movies Name</label>
                <input type="text" name="movies_name"  class="form-control" placeholder="enter the name">
            </div>
           
        </div>
        <div class="row mb-3">
            <div class="col-md-6">
                <label for="">URL Name</label>
                <input type="text" class="form-control" name="url_name" placeholder="enter the url name">
                
            </div>
          
        </div>

   
        
        
        <div class="row mb-3">
            <div class="col-md-6">
                <label for="category">Categories</label>
                <select name="category" id="category" class="form-control">
                    @foreach($category as $category)
                    <option value="{{ $category->category_name }}">{{ $category->category_name }}</option>
                    @endforeach
                  </select>
             
            </div>
          
        </div>

        <div class="row mb-3">
            <div class="col-md-6">
                <label for="genres">Genres</label>
                <select name="genres" id="genres" class="form-control">
                    @foreach($genres as $genres)
                    <option value="{{ $genres->generes_name }}">{{ $genres->generes_name }}</option>
                    @endforeach
                  </select>
             
            </div>
          
        </div>


        <div class="row mb-3">
            <div class="col-md-6">
                <label for="director">စီစဉ်သူ</label>
                <input type="text" class="form-control" id="director" name="director" placeholder="စီစဉ်သူနာမည်">
                
            </div>
          
        </div>

        
        <div class="row mb-3">
            <div class="col-md-6">
                <label for="actors">သရုပ်ဆောင်</label>
                <input type="text" class="form-control" id="actors" name="actors" placeholder="သရုပ်ဆောင်နာမည်">
                
            </div>
          
        </div>

        <div class="row mb-3">
            <div class="col-md-6">
                <label for="actors">ထုတ်လုပ်သူ</label>
                <input type="text" class="form-control" id="producer" name="producer" placeholder="ထုတ်လုပ်သူနာမည်">
                
            </div>
          
        </div>
 
        <div class="row mb-3">
            <div class="col-md-6">
                <label for="production_date">Production Date</label>
                <input type="date" class="form-control" id="production_date" name="production_date">
                
            </div>
          
        </div>

        <div class="row mb-3">
            <div class="col-md-6">
                <label for="posted_date">Posted Date</label>
                <input type="date" class="form-control" id="posted_date" name="posted_date">
                
            </div>
          
        </div>
 
        
        @push('scripts')
   <script type="text/javascript">
           $('#production_date').datetimepicker({
               format: 'YYYY-MM-DD',
               sideBySide: true
           })
       </script>
@endpush

@push('scripts')
   <script type="text/javascript">
           $('#posted_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush



<div class="row mb-3">
    <div class="col-md-6">
        <label for="description">Description</label>
        <textarea name="description" id="description"  class="form-control" cols="30" rows="10" placeholder="description"></textarea>
    </div>
  
</div>



<div class="row mb-3">
    <div class="col-md-6">
        <label for="has_season">Has Season</label>
        <select name="has_season" id="has_season" class="form-control">
          <option value="0">No</option>
          <option value="1">Yes</option>
          </select>
     
    </div>
  
</div>



<div class="row mb-3">
    <div class="col-md-6">
        <label for="">Season Id</label>
        <select name="season_id" id="season_id" class="form-control">
            <option value="0">Don't have Season</option>
            @foreach($season as $season)
                    <option value="{{ $season->id }}">{{ $season->season_name }}</option>
             @endforeach
        </select>
        
    </div>
  
</div>







<div class="row mb-3">
       
    <div class="col-md-6">
        <label for="thumbnail_image">Preview ပေါ်မည့်ပုံ ထည့်သွင်းပေးရန်</label>
        <input type="file" name="thumbnail_image" id="thumbnail_image" class="form-control">
    </div>
   
</div>

<div class="row mb-3">
       
    <div class="col-md-6">
        <label for="myday_img">MyDay ပေါ်မည့်ပုံ ထည့်သွင်းပေးရန်</label>
        <input type="file" name="myday_img" id="myday_img" class="form-control">
    </div>
   
</div>



<div class="row mb-3">
       
    <div class="col-md-6">
        <label for="vimeo_url">Vimeo Url ထည့်သွင်းပေးရန်</label>
        <input type="text" name="vimeo_url" id="vimeo_url" class="form-control">
    </div>
   
</div>

<div class="row mb-3">
       
    <div class="col-md-6">
        <label for="youtube_url">Youtube Url ထည့်သွင်းရန်</label>
        <input type="text" name="youtube_url" id="youtube_url" class="form-control">
    </div>
   
</div>

<div class="row mb-3">
    <div class="col-md-6">
        <label for="">Channel ရွေးချယ်ရန်</label>
        <select name="option" id="option" class="form-control">
            @foreach($channel as $channel)
            <option value="{{ $channel->channel_name }}">{{ $channel->channel_name }}</option>
            @endforeach
        </select>
        
    </div>
  
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label for="visible">Visible:</label>
        <select class="form-control" id="visible" name="visible">
            <option value="1">yes</option>
            <option value="0">no</option>
        </select>
    </div>

</div>



        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

    
@endsection