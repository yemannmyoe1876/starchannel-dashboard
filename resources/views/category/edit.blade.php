@extends('layouts.app')

@section('contents')

<div class="container mt-5">
    <div class="col-md-12">
        <h1>Category</h1>
        <form action="{{ route('category.update', $category->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group mt-4">
                <label for="">Category Name</label>
                <input type="text" class="form-control" value="{{ $category->category_name }}" name="category_name">
            </div>
            <div class="form-group mt-4">
                <label for="">Type</label>
                <input type="text" class="form-control" value="{{ $category->type }}" name="type">
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <input type="text" class="form-control"  value="{{ $category->status }}" name="status">
               
            </div>
            <div class="form-group">
                <label for="">Option</label>
                <input type="text" class="form-control" value="{{ $category->option }}" name="option">
            </div>

            <div class="form-group">
                <label for="">Visible</label>
                <input type="text" class="form-control" value="{{ $category->visible }}" name="visible">
            </div>

            <button type="submit" class="btn btn-success float-right">Update</button>
        </form>
    </div>
</div>

@section('scripts')
    <script>
        $(function(){
            $('.channel_logo_update').on('change',function(){
                var channel_url = window.URL.createObjectURL(this.files[0]);
                $('.channel_view').html('<img id="channel_preview" src="'+ channel_url +'" class="mt-3 text-center" alt="" width="400" height="400">');
            });
        })
    </script>
@endsection
@endsection