@extends('layouts.app')
  
@section('title', 'Show Category')
  
@section('contents')
    <h1 class="mb-0">Detail Category</h1>

    <hr />
    <div class="row mb-3">
        <div class="col-md-6">
            <label class="form-label">Category</label>
            <input type="text" name="category_name" class="form-control" placeholder="category_name" value="{{ $category->category_name }}" readonly>
        </div>
     
    </div>

    <div class="row mb-3">
        <div class="col-md-6">
            <label class="form-label">Type</label>
            <input type="text" name="type" class="form-control" value="{{ $category->type }}" readonly>
        </div>
    </div>





    <div class="row mb-3">
        <div class="col-md-6">
            <label class="form-label">Status</label>
            <input type="read" name="status" class="form-control"  value="{{ $category->status }}" readonly>
 
        </div>
       
    </div>

<div class="row mb-3">
    <div class="col-md-6">
        <label class="form-label">Option</label>
        <input type="read" name="option" class="form-control"  value="{{ $category->option }}" readonly>
    </div>

</div>
<div class="row mb-3">
    <div class="col-md-6">
        <label class="form-label">Visible</label>
        <input type="read" name="visible" class="form-control"  value="{{ $category->visible }}" readonly>
    </div>

</div>

    <div class="row mb-3">
        <div class="col-md-6">
            <label class="form-label">Created At</label>
            <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $category->created_at }}" readonly>
        </div>
     
    </div>
<div class="row mb-3">
    <div class="col-md-6">
        <label class="form-label">Updated At</label>
        <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $category->updated_at }}" readonly>
    </div>
</div>
@endsection