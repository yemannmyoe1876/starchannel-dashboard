@extends('layouts.app')
  
@section('title', 'Show Season')
  
@section('contents')
    <h1 class="mb-0">Detail Season</h1>

    <hr />
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Season</label>
            <input type="text" name="season_name" class="form-control"  value="{{ $season->season_name }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Type</label>
            <input type="text" name="type" class="form-control" value="{{ $season->type }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Status</label>
            <input type="read" name="status" class="form-control"  value="{{ $season->status }}" readonly>
     

           

            
        </div>
        <div class="col mb-3">
            <label class="form-label">Option</label>
            <input type="read" name="option" class="form-control"  value="{{ $season->option }}" readonly>
        </div>
{{-- 
        <div class="col mb-3">
            <label class="form-label">Visible</label>
            <input type="read" name="visible" class="form-control"  value="{{ $category->visible }}" readonly>
        </div> --}}
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Created At</label>
            <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $season->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Updated At</label>
            <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $season->updated_at }}" readonly>
        </div>
    </div>
    
@endsection