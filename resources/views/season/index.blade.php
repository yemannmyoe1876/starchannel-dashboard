@extends('layouts.app')
  

  
@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Season Lists</h1>
        <a href="{{ route('season.create') }}" class="btn btn-primary">Add Season</a>
    </div>
    <hr />
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    <table class="table table-hover">
        <thead class="table-primary">
            <tr>
                <th>#</th>
                <th>Season_name</th>
                <th>Type</th>
                <th>Status</th>
                <th>Option</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>+
            @if($season->count() > 0)
                @foreach($season as $season)
                    <tr>
                        <td class="align-middle">{{ $loop->iteration }}</td>
                        <td class="align-middle">{{ $season->season_name }}</td>
                        <td class="align-middle">{{ $season->type }}</td>
                        <td class="align-middle">{{ $season->status }}</td>
                        <td class="align-middle">{{ $season->option }}</td>  
                       
                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="{{ route('season.show', $season->id) }}" type="button" class="btn btn-secondary">Detail</a>
                                <a href="{{ route('season.edit', $season->id)}}" type="button" class="btn btn-warning">Edit</a>
                                <form action="{{ route('season.destroy', $season->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger m-0">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">data not found</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection