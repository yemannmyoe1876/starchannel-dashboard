@extends('layouts.app')
  
@section('title', 'Create generes')
  
@section('contents')
  
    <hr />
    <form action="{{route('generes.store')}}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row mb-4">
       
            <div class="col">
                <label for="name">Generes Name</label>
    <input class="form-control" name="generes_name" type="text" id="generes">
            </div>
            <div class="col">
                <label for="">Type</label>
                <input type="text" name="type" class="form-control" placeholder="enter type" required>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col">
                <label for="">Status</label>
                <input type="text" class="form-control channel_logo" name="status" placeholder="enter the status" required>
                
            </div>
            <div class="col">
                <label for="">Option</label>
                <input type="text" name="option" class="form-control" placeholder="entet the option" required>
            </div>
        </div>

    {{-- <div class="row">
        <div class="form-group col-sm-6">
            <label for="visible">Visible:</label>
            <select class="form-control" id="visible" name="visible"><option value="1">yes</option><option value="0">no</option></select>
        </div>

    </div> --}}
    <br>
        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection