@extends('layouts.app')

@section('contents')

<div class="container mt-5">
    <div class="col-md-12">
        <h1>Generes</h1>
        <form action="{{ route('generes.update', $generes->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group mt-4">
                <label for="">Genres Name</label>
                <input type="text" class="form-control" value="{{ $generes->generes_name }}" name="generes_name">
            </div>
            <div class="form-group mt-4">
                <label for="">Type</label>
                <input type="text" class="form-control" value="{{ $generes->type }}" name="type">
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <input type="text" class="form-control"  value="{{ $generes->status }}" name="status">
               
            </div>
            <div class="form-group">
                <label for="">Option</label>
                <input type="text" class="form-control" value="{{ $generes->option }}" name="option">
            </div>

          

            <button type="submit" class="btn btn-success float-right">Update</button>
        </form>
    </div>
</div>

@section('scripts')
    <script>
        $(function(){
            $('.channel_logo_update').on('change',function(){
                var channel_url = window.URL.createObjectURL(this.files[0]);
                $('.channel_view').html('<img id="channel_preview" src="'+ channel_url +'" class="mt-3 text-center" alt="" width="400" height="400">');
            });
        })
    </script>
@endsection
@endsection