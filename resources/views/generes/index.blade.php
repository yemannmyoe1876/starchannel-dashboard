@extends('layouts.app')
  

  
@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Generes Lists</h1>
        <a href="{{ route('generes.create') }}" class="btn btn-primary">Add Generes</a>
    </div>
    <hr />
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    <table class="table table-hover">
        <thead class="table-primary">
            <tr>
                <th>#</th>
                <th>Generes_name</th>
                <th>Type</th>
                <th>Status</th>
                <th>Option</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>+
            @if($generes->count() > 0)
                @foreach($generes as $generes)
                    <tr>
                        <td class="align-middle">{{ $loop->iteration }}</td>
                        <td class="align-middle">{{ $generes->generes_name }}</td>
                        <td class="align-middle">{{ $generes->type }}</td>
                        <td class="align-middle">{{ $generes->status }}</td>
                        <td class="align-middle">{{ $generes->option }}</td>  
                       
                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="{{ route('generes.show', $generes->id) }}" type="button" class="btn btn-secondary">Detail</a>
                                <a href="{{ route('generes.edit', $generes->id)}}" type="button" class="btn btn-warning">Edit</a>
                                <form action="{{ route('generes.destroy', $generes->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger m-0">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">data not found</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection