@extends('layouts.app')
  
@section('title', 'Show Generes')
  
@section('contents')
    <h1 class="mb-0">Detail Generes</h1>

    <hr />
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Generes</label>
            <input type="text" name="generes_name" class="form-control" placeholder="category_name" value="{{ $generes->genesres_name }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Type</label>
            <input type="text" name="type" class="form-control" value="{{ $generes->type }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Status</label>
            <input type="read" name="status" class="form-control"  value="{{ $generes->status }}" readonly>
     

           

            
        </div>
        <div class="col mb-3">
            <label class="form-label">Option</label>
            <input type="read" name="option" class="form-control"  value="{{ $generes->option }}" readonly>
        </div>
{{-- 
        <div class="col mb-3">
            <label class="form-label">Visible</label>
            <input type="read" name="visible" class="form-control"  value="{{ $category->visible }}" readonly>
        </div> --}}
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Created At</label>
            <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $generes->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Updated At</label>
            <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $generes->updated_at }}" readonly>
        </div>
    </div>
    
@endsection