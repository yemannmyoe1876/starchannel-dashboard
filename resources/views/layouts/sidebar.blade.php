<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
      </div>
      <div class="sidebar-brand-text mx-3">Admin</div>
    </a>
    
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href="{{ route('dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>
    
    <li class="nav-item">
      <a class="nav-link" href="{{route('channel')}}">
        <i class="fas fa-fw fa-solid fa-tv"></i>
        
        <span>Channel</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('category')}}">
        <i class="fas fa-fw fa-solid fa-inbox"></i>
        <span>Category(sidebar)</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('generes')}}">
        <i class="fas fa-fw fa-solid fa-stethoscope"></i>
        <span>Generes</span></a>
    </li>


    <li class="nav-item">
      <a class="nav-link" href="{{route('season')}}">
        <i class="fas fa-fw fa-solid fa-wind"></i>
        <span>Season</span></a>
    </li>


    <li class="nav-item">
      <a class="nav-link" href="{{route('movies')}}">
        <i class="fas fa-fw fa-solid fa-video"></i>
        <span>Movies</span></a>
    </li>
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    
    
  </ul>