@extends('layouts.app')

@section('contents')

<div class="container mt-5">
    <div class="col-md-12">
        <h1>Channel</h1>
        <form action="{{ route('channel.update', $channel->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group mt-4">
                <label for="">Channel Name</label>
                <input type="text" class="form-control" value="{{ $channel->channel_name }}" name="channel_name">
            </div>
            <div class="form-group mt-4">
                <label for="">Channel Name MM</label>
                <input type="text" class="form-control" value="{{ $channel->channel_name_mm }}" name="channel_name_mm">
            </div>
            <div class="form-group">
                <label for="">Channel Logo</label>
                <input type="file" class="form-control channel_logo_update" name="channel_logo">
                <div class="channel_view text-center">
                    
                </div>
            </div>
            <div class="form-group">
                <label for="">Keyword</label>
                <input type="text" class="form-control" value="{{ $channel->keyword }}" name="keyword">
            </div>
            <button type="submit" class="btn btn-success float-right">Update</button>
        </form>
    </div>
</div>

@section('scripts')
    <script>
        $(function(){
            $('.channel_logo_update').on('change',function(){
                var channel_url = window.URL.createObjectURL(this.files[0]);
                $('.channel_view').html('<img id="channel_preview" src="'+ channel_url +'" class="mt-3 text-center" alt="" width="400" height="400">');
            });
        })
    </script>
@endsection
@endsection