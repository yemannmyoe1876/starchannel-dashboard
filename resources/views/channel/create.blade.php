@extends('layouts.app')
  
@section('title', 'Create channel')
  
@section('contents')
  
    <hr />
    <form action="{{route('channel.store')}}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row mb-3">
       
            <div class="col">
                <label for="">Channel Name</label>
                <input type="text" name="channel_name" id="option" class="form-control" placeholder="enter the name">
            </div>
            <div class="col">
                <label for="">Channel Name MM</label>
                <input type="text" name="channel_name_mm" class="form-control" placeholder="name_mm">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <label for="">Channel Logo</label>
                <input type="file" class="form-control channel_logo" name="channel_logo">
                
            </div>
            <div class="col">
                <label for="">KeyWord</label>
                <input type="text" name="keyword" class="form-control" placeholder="keyword">
            </div>
        </div>
 
        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection