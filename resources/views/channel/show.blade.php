@extends('layouts.app')
  
@section('title', 'Show channel')
  
@section('contents')
    <h1 class="mb-0">Detail Channel</h1>

    <hr />
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Channel</label>
            <input type="text" name="channel_name" class="form-control" placeholder="channel_name" value="{{ $channel->channel_name }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Channel_name_mm</label>
            <input type="text" name="channel_name_mm" class="form-control" placeholder="channel_name_mm" value="{{ $channel->channel_name_mm }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Channel_logo</label>
            <input type="read" name="channel_logo" class="form-control" placeholder="channel_logo" value="{{ $channel->channel_logo }}" readonly>
            {{-- <img src="{{asset($channel->channel_logo)}}" width="80" height="80" alt=""> --}}

           

            
        </div>
        <div class="col mb-3">
            <label class="form-label">KeyWord</label>
            <textarea class="form-control" name="description" placeholder="keyword" readonly>{{ $channel->keyword }}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Created At</label>
            <input type="text" name="created_at" class="form-control" placeholder="Created At" value="{{ $channel->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Updated At</label>
            <input type="text" name="updated_at" class="form-control" placeholder="Updated At" value="{{ $channel->updated_at }}" readonly>
        </div>
    </div>
    
@endsection