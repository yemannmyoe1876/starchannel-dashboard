@extends('layouts.app')
  
@section('title', 'Home channel')
  
@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Channel List</h1>
        <a href="{{ route('channel.create') }}" class="btn btn-primary">Add Channel</a>
    </div>
    <hr />

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif

    <table class="table table-hover">
        <thead class="table-primary">
            <tr>
                <th>#</th>
                <th>Channel</th>
                <th>Channel_name_mm</th>
                <th>Channe_logo</th>
                <th>keyword</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>+
            @if($channel->count() > 0)
                @foreach($channel as $channel)
                    <tr>
                        <td class="align-middle">{{ $loop->iteration }}</td>
                        <td class="align-middle">{{ $channel->channel_name }}</td>
                        <td class="align-middle">{{ $channel->channel_name_mm }}</td>
                        <td class="align-middle">{{ $channel->channel_logo }}</td>
                        <td class="align-middle">{{ $channel->keyword }}</td>  
                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                 <a href="{{route('channel.show', $channel->id)}}" type="button" class="btn btn-info">Detail</a>
                                <a href="{{route('channel.edit', $channel->id)}}" type="button" class="btn btn-warning">Edit</a>
                                <form action="{{ route('channel.destroy', $channel->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')


                                <button class="btn btn-danger m-0">Delete</button>
                              
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">channel not found</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection