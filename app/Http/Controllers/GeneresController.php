<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\generes;



class GeneresController extends Controller
{
    public function index(){
        $generes = Generes::orderBy('created_at', 'DESC')->get();
        return view('generes.index', compact('generes'));
    }

    
public function create()
{
    return view('generes.create');
}



public function store(Request $request)
    {
        $generes_name = $request->generes_name;
        $type = $request->type;
        $status = $request->status;
        $option = $request->option;
       

        $generes = new Generes;
        $generes->generes_name = $generes_name;
        $generes->type = $type;
   
        $generes->status = $status;
        $generes->option = $option;
        $generes->save();

        return redirect()->route('generes')->with('success', 'generes added successfully'); 
    }



    public function show(string $id)
    {
        {
            $generes = Generes::findOrFail($id);
    
            return view('generes.show', compact('generes'));
        }
    }

    public function edit($id)
    {
        $generes = Generes::findOrFail($id);
        return view('generes.edit', compact('generes'));
    }


    public function update(Request $request, $id)
    {
        $generes_name = $request->generes_name;
        $type = $request->type;
        $status = $request->status;
        $option = $request->option;
     


        $generes = Generes::findOrFail($id);
        $generes->generes_name = $generes_name;
        $generes->type = $type;
        $generes->status = $status;
        $generes->option = $option;

      
      
        $generes->save();

        return redirect()->route('generes', $generes->id)->with('success', 'updated successfully');
    }

    public function destroy($id)
    {
        $generes = Generes::findOrFail($id);
        $generes->delete();

        return redirect()->route('generes')->with('success', 'deleted successfully');
    }

}

