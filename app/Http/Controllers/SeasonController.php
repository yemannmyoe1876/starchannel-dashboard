<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Season;

class SeasonController extends Controller
{
    public function index(){
        $season = season::orderBy('created_at', 'DESC')->get();
        return view('season.index', compact('season'));

     
    }


    public function create()
{
    return view('season.create');
}



    public function store(Request $request)
    {
        $season_name = $request->season_name;
        $type = $request->type;
        $status = $request->status;
        $option = $request->option;
       

        $season = new Season;
        $season->season_name = $season_name;
        $season->type = $type;
   
        $season->status = $status;
        $season->option = $option;
        $season->save();

        return redirect()->route('season')->with('success', 'season added successfully'); 
    }


    public function show(string $id)
    {
        {
            $season = Season::findOrFail($id);
    
            return view('season.show', compact('season'));
        }
    }


    public function edit($id)
    {
        $season = Season::findOrFail($id);
        return view('season.edit', compact('season'));
    }


    public function update(Request $request, $id)
    {
        $season_name = $request->season_name;
        $type = $request->type;
        $status = $request->status;
        $option = $request->option;
      
        

        $season = new Season;
        $season->season_name = $season_name;
        $season->type = $type;
   
        $season->status = $status;
        $season->option = $option;
      
        $season->save();

        return redirect()->route('season', $season->id)->with('success', 'updated successfully');
    }

    public function destroy($id)
    {
       

        $season = Season::findOrFail($id);
        $season->delete();
    
        return redirect()->route('season')->with('success', 'deleted successfully');
    
    }

}
