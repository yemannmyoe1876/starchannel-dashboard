<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = DB::table('movies')
        ->select(
            'id',
            'name',
            'categories',
            'genres',
            'director',
            'actors',
            'producer',
            'production_date',
            'description',
            'thumbnail_image',
            'images',
            'vimeo_url',
            'view_count',
            'myday_img'
            )->where([['view_count','!=', null], ['deleted_at', null]])->orderby('view_count','desc')->limit(10)->get();

            $editor_choice = DB::table('movies')->where('categories',2)
            ->select(
                'id',
                'name',
                'categories',
                'genres',
                'director',
                'actors',
                'producer',
                'production_date',
                'description',
                'thumbnail_image',
                'images',
                'vimeo_url')->where('deleted_at', null)->orderby('id','desc')->limit(2)->get();
	return response()->json($data,200); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
