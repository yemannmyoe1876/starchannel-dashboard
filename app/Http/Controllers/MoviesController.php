<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movies;
use App\Models\Channel;
use App\Models\Category;
use App\Models\Generes;
use App\Models\season;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use DB;


class MoviesController extends Controller
{
    public function index(){
        $movies = Movies::where('deleted_at',null)->orderby('production_date','desc')->orderby('created_at', 'desc')->paginate(10);
        $channel = Channel::all();
        $array = [];
        foreach($movies as $movie){
            // $channel = Channel::where('keyword', '=', $movie->option)->first();
            // if($channel !== null){
                $data = [
                    "id" => $movie->id,
                    "movies_name" => $movie->name,
                    "visible" => $movie->visible,
                    "vimeo_url" => $movie->vimeo_url,
                    "production_date" => $movie->production_date,
                    "channel" => $movie->option
                ];
                array_push($array, $data);
            // }
        }
        // dd($movies, $arrays);

        return view('movies.index', compact('array'))->with('movies', $movies,'i', (request()->input('page',1)- 1) * 5);
    }
    


    public function search(Request $request){
        $movies = Movies::where([
            ['name', '!=', NULL],
            [function($query) use ($request){
                if (!empty(($request))) {
                    if(!empty($request->searchdate)){
                        $ex = explode('/', $request->searchdate);
                        $date = $ex[2] . '-' . $ex[0] . '-' . $ex[1];
                        $d = date("Y-m-d", strtotime($date));
                        $query->orWhere('production_date', 'LIKE', '%' . $d . '%')->get();
                    } else {
                        $query->orWhere('name', 'LIKE', '%' . $request->term . '%')->get();
                    }
                }
            }]
        ])->orderBy('id', 'desc')
          ->paginate(10);

    $array = [];
    foreach($movies as $movies){
        $channel = Channel::where('keyword', '=', $movie->option)->first();
        if($channel !== null){
            $data = [
                "id" => $movies->id,
                "name" => $movies->name,
                "visible" => $movies->visible,
                "vimeo_url" => $movies->vimeo_url,
                "production_date" => $movies->production_date,
                "channel" => $channel->option
            ];
            array_push($array, $data);
        }
    }

        return view('movies.index', compact('array'))->with('movies', $movies,'i', (request()->input('page',1)- 1) * 5);
    }

    public function create()
    {
        $channel = Channel::orderby('id', 'DESC')->get();
        $seasons = DB::table('season')->where('deleted_at',null)->get();
        $category = DB::table('categories')->where('deleted_at',null)->get();
        $genres = DB::table('generes')->where('deleted_at',null)->get();
     
        $data = [
            'season'=>$seasons,
            'channel' => $channel,
            'category' => $category,
            'genres' => $genres,
          
            
        ];
        return view('movies.create',$data);
    }


    public function store( Request $request)
    {
        $movies = new Movies;
        $movies->name = $request->movies_name;
        $movies->url_name = $request->url_name;
        $movies->categories = $request->category;
        $movies->genres = $request->genres;
        $movies->director = $request->director;
        $movies->actors = $request->actors;
        $movies->producer = $request->producer;
        $movies->production_date = $request->production_date;
        $movies->posted_date = $request->posted_date;
        $movies->description = $request->description;
        $movies->vimeo_url = $request->vimeo_url;
        $movies->youtube_url = $request->youtube_url;
        $movies->visible = $request->visible;
        $movies->channel = $request->option;
        $movies->has_season = $request->has_season;
        $movies->season_id = $request->season_id;
     
        $image = $request->file('thumbnail_image');
        $myday_image = $request->file('myday_img');
        if($myday_image){
            $name = Str::slug($request->input('name')).'_'.time();
            $folder = 'uploads/myday_images/';
            $filePathMyday = $folder . $name. '.' . $myday_image->getClientOriginalExtension();
            // $this->uploadOne($myday_image, $folder, 'public', $name);
            $movies->myday_img = '/storage'. $filePathMyday;
        }
        if($image){
            $name = Str::slug($request->input('name')).'_'.time();
            $folder = 'uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // $this->uploadOne($image, $folder, 'public', $name);
            $movies->thumbnail_image = '/storage'.$filePath;
            $movies->images = '/storage'.$filePath;
           
        }
        $movies->save();

        //    Flash::success('Movies saved successfully.');
        // //notification
        // $channel = Channel::where('keyword', '=', $request->option)->first();
        // $type = 'test';
        // $newId = $movies->id;
        // $channel_name = $channel->channel_name;
        // $channel_keyword = $channel->keyword;
        // $notification = new NotificationController;
		// $send_noti = $notification->notification($newId,$request->name,$channel_name,$channel_keyword,$type);

        // return redirect(route('movies.index'));
	

   

       

        return redirect()->route('movies')->with('success', 'Movies added successfully'); 
    }

    public function show($id)
    {
         $movies = Movies::findOrFail($id);
         $channel = Channel::all();
         return view('movies.show', compact('movies' , 'channel'));

    }

    public function edit($id)
    {
        $movies = Movies::findOrFail($id);
        $channel =Channel::all();
        $category = Category::all();
        $generes = Generes::all();
        $season = Season::all();
        $format_prod_date = $movies->production_date->format('Y-m-d');
        $format_post_date = $movies->posted_date->format('Y-m-d');
        // dd($movies);

        return view('movies.edit', compact('movies', 'channel', 'category' , 'generes' ,'format_prod_date','format_post_date', 'season'));  
    }


    public function update(Request $request, string $id)
    {
        $movies = Movies::findorFail($id);
        $movies->name = $request->movies_name;
        $movies->url_name = $request->url_name;
        $movies->categories = $request->category;
        $movies->genres = $request->genres;
        $movies->director = $request->director;
        $movies->actors = $request->actors;
        $movies->producer = $request->producer;
        $movies->production_date = $request->production_date;
        $movies->posted_date = $request->posted_date;
        $movies->description = $request->description;
        $movies->vimeo_url = $request->vimeo_url;
        $movies->youtube_url = $request->youtube_url;
        $movies->visible = $request->visible;
        $movies->has_season = $request->has_season;
        $movies->season_id = $request->season_id;
        $movies->channel = $request->option;
    
        $image = $request->file('thumbnail_image');
        $myday_image = $request->file('myday_img');
        if($myday_image){
            $name = Str::slug($request->input('name')).'_'.time();
            $folder = 'uploads/myday_images/';
            $filePathMyday = $folder . $name. '.' . $myday_image->getClientOriginalExtension();
            // $this->uploadOne($myday_image, $folder, 'public', $name);
            $movies->myday_img = '/storage'. $filePathMyday;
        }
        if($image){
            $name = Str::slug($request->input('name')).'_'.time();
            $folder = 'uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // $this->uploadOne($image, $folder, 'public', $name);
            $movies->thumbnail_image = '/storage'.$filePath;
            $movies->images = '/storage'.$filePath;
           
        }
        $movies->update();

    //    $request->validate([
    //     "movies" => 'required',
    //     "url_name" => 'required',
    //     "categories" => 'required',
    //     "genres" => 'required',
    //     "director" =>  'required',
    //     "actors" => 'required',
    //     "producer" => 'required',
    //     "production_date" => 'required',
    //     "posted_date" => 'required',
    //     "description" => 'required',
    //     "vimeo_url" => 'required',
    //     "youtube_url" => 'required',
    //     "visible" => 'required',
    //     "has_season" => 'required',
    //     "season_id" => 'required',
    //     "channel" => 'required',

    //    ]);



        return redirect()->route('movies')->with('success', 'Movies update successfully'); 
    }

  
    public function destroy($id)
    {
        $movies = Movies::findOrFail($id);
        $movies->delete();

        return redirect()->route('movies')->with('success', 'deleted successfully');
    }

}
