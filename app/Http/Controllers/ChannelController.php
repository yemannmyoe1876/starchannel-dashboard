<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Channel;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channel = Channel::orderBy('created_at', 'DESC')->get();
        return view('channel.index', compact('channel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('channel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    // {
    //     Channel::create($request->all());

    //     return redirect()->route('channel')->with('success', 'Channel added successfully');
    // }

    {
        $channel_name = $request->channel_name;
        $channel_name_mm = $request->channel_name_mm;
        $keyword = $request->keyword;
        $channel_logo = $request->file('channel_logo');

        $channel = new Channel;
        $channel->channel_name = $channel_name;
        $channel->channel_name_mm = $channel_name_mm;
        if($request->hasfile('channel_logo')){
            $imageName = $channel_logo->getClientOriginalName();
            $file_save = $channel_logo->storeAs('channel_images', $imageName, 'public');
            $channel->channel_logo = $imageName;
        } 
        $channel->keyword = $keyword;
        $channel->save();

        return redirect()->route('channel')->with('success', 'Channel added successfully'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $channel = Channel::findOrFail($id);

        return view('channel.show', compact('channel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $channel = Channel::findOrFail($id);
        return view('channel.edit', compact('channel'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $channel_name = $request->channel_name;
        $channel_name_mm = $request->channel_name_mm;
        $keyword = $request->keyword;
        $channel_logo = $request->file('channel_logo');

        $channel = Channel::findOrFail($id);
        $channel->channel_name = $channel_name;
        $channel->channel_name_mm = $channel_name_mm;
        if($request->hasfile('channel_logo')){
            $imageName = $channel_logo->getClientOriginalName();
            $file_save = $channel_logo->storeAs('channel_images', $imageName, 'public');
            $channel->channel_logo = $imageName;
        } 
        $channel->keyword = $keyword;
        $channel->save();

        return redirect()->route('channel', $channel->id)->with('success', 'updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $channel = Channel::findOrFail($id);
        $channel->delete();

        return redirect()->route('channel')->with('success', 'deleted successfully');
    }
    
}
