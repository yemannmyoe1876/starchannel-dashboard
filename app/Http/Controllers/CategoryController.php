<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::orderBy('created_at', 'DESC')->get();
        return view('category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category_name = $request->category_name;
        $type = $request->type;
        $status = $request->status;
        $option = $request->option;
        $visible = $request->visible;
        

        $category = new Category;
        $category->category_name = $category_name;
        $category->type = $type;
   
        $category->status = $status;
        $category->option = $option;
        $category->visible = $visible;
        $category->save();

        return redirect()->route('category')->with('success', 'category added successfully'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        {
            $category = Category::findOrFail($id);
    
            return view('category.show', compact('category'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category_name = $request->category_name;
        $type = $request->type;
        $status = $request->status;
        $option = $request->option;
        $visible = $request->visible;
        

        $category = Category::findOrFail($id);
        $category->category_name = $category_name;
        $category->type = $type;
   
        $category->status = $status;
        $category->option = $option;
        $category->visible = $visible;
        $category->save();

        return redirect()->route('category', $category->id)->with('success', 'updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return redirect()->route('category')->with('success', 'deleted successfully');
    }
}
