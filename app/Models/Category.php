<?php

namespace App\Models;

use App\Models\Movies;
use Eloquent as Model;
use App\Http\Controllers\MoviesController;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version April 23, 2020, 2:45 am UTC
 *
 * @property string name
 * @property string type
 * @property string status
 * @property string option
 * @property select visible
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'Category_name',
        'type',
        'status',
        'option',
        'visible'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'Category_name' => 'string',
        'type' => 'string',
        'status' => 'string',
        'option' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function movie()
    {
        return $this->belongsTo(Movies::class);
    }
    
}
