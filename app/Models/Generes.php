<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class generes extends Model
{
    use HasFactory;

    public $table = 'generes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'generes_name',
        'type',
        'status',
        'option',
      
    ];

    protected $casts = [ 
        'id' => 'integer',
        'generes_name' => 'string',
        'type' => 'string',
        'status' => 'string',
        'option' => 'string'
    ];
}
