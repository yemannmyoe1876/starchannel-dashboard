<?php

namespace App\Models;
use App\Models\category;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    use HasFactory;

    

    public $table = 'movies';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'url_name',
        'categories',
        'genres',
        'director',
        'actors',
        'producer',
        'production_date',
        'posted_date',
        'description',
        'has_season',
        'season_id',
        'thumbnail_image',
        'images',
        'features_image',
        'vimeo_url',
        'youtube_url',
        'status',
        'type',
        'option',
        'visible'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'url_name' => 'string',
        'categories' => 'string',
        'genres' => 'string',
        'director' => 'string',
        'actors' => 'string',
        'producer' => 'string',
        'production_date' => 'date',
        'posted_date' => 'date',
        'description' => 'string',
        'has_season' => 'string',
        'season_id' => 'string',
        'thumbnail_image' => 'string',
        'images' => 'string',
        'features_image' => 'string',
        'vimeo_url' => 'string',
        'youtube_url' => 'string',
        'channel' => 'string',
        'visible' => 'string'
    ];

    
    public function categories()
    {
        return $this->hasMany(Category::class);
    }
}
