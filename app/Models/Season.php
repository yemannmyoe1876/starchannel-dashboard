<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class Season extends Model
{
    use HasFactory;

    

    public $table = 'season';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'season_name',
        'type',
        'status',
        'option'
    
    ];


    protected $casts = [
        'id' => 'integer',
        'season_name' => 'string',
        'type' => 'string',
        'status' => 'string',
        'option' => 'string',
        'visible' => 'string'
    ];
}
