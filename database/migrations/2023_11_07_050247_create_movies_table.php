<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('url_name')->nullable();
            $table->string('categories')->nullable();
            $table->string('genres')->nullable();
            $table->string('director')->nullable();
            $table->string('actors')->nullable();
            $table->string('producer')->nullable();
            $table->date('production_date')->nullable();
            $table->date('posted_date')->nullable();
            $table->text('description')->nullable();
            $table->text('has_season')->nullable();
            $table->string('season_id')->nullable();
            $table->text('thumbnail_image')->nullable();
            $table->text('images')->nullable();
            $table->text('myday_img')->nullable();
            $table->text('features_image')->nullable();
            $table->text('vimeo_url')->nullable();
            $table->text('youtube_url')->nullable();
            $table->string('channel')->nullable();
            $table->string('visible')->nullable();
            $table->string('view_count')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('movies');
    }
};
