<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\GeneresController;
use App\Http\Controllers\SeasonController;
use App\Http\Controllers\MoviesController;






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('movies', [HomeApiController::class,'index']);


Route::controller(AuthController::class)->group(function () {
    Route::get('register', 'register')->name('register');
    Route::post('register', 'registerSave')->name('register.save');

    Route::get('login', 'login')->name('login');
    Route::post('login', 'loginAction')->name('login.action');


    Route::get('logout', 'logout')->middleware('auth')->name('logout');
});


Route::middleware('auth')->group(function () {
    Route::get('dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
 







    Route::controller(ChannelController::class)->prefix('channel')->group(function (){
        Route::get('', 'index')->name('channel');
        Route::get('create', 'create')->name('channel.create');
        Route::post('store', 'store')->name('channel.store');
        Route::get('show/{id}', 'show')->name('channel.show');
        Route::get('edit/{id}', 'edit')->name('channel.edit');
        Route::put('edit/{id}', 'update')->name('channel.update');
        Route::delete('destroy/{id}', 'destroy')->name('channel.destroy');
    });

    
    Route::controller(CategoryController::class)->prefix('category')->group(function (){
        Route::get('', 'index')->name('category');
        Route::get('create', 'create')->name('category.create');
        Route::post('store', 'store')->name('category.store');
        Route::get('show/{id}', 'show')->name('category.show');
        Route::get('edit/{id}', 'edit')->name('category.edit');
        Route::put('edit/{id}', 'update')->name('category.update');
        Route::delete('destroy/{id}', 'destroy')->name('category.destroy');
    });


    Route::controller(GeneresController::class)->prefix('generes')->group(function (){
        Route::get('/', 'index')->name('generes');
        Route::get('create', 'create')->name('generes.create');
        Route::post('store', 'store')->name('generes.store');
        Route::get('show/{id}', 'show')->name('generes.show');
        Route::get('edit/{id}', 'edit')->name('generes.edit');
        Route::put('edit/{id}', 'update')->name('generes.update');
        Route::delete('destroy/{id}', 'destroy')->name('generes.destroy');
    });


    
    Route::controller(SeasonController::class)->prefix('seasons')->group(function (){
        Route::get('/', 'index')->name('season');
        Route::get('create', 'create')->name('season.create');
        Route::post('store', 'store')->name('season.store');
        Route::get('show/{id}', 'show')->name('season.show');
        Route::get('edit/{id}', 'edit')->name('season.edit');
        Route::put('edit/{id}', 'update')->name('season.update');
        Route::delete('destroy/{id}', 'destroy')->name('season.destroy');
    });


    // Route::get('movies', [MoviesController::class,'index']);

    Route::controller(MoviesController::class)->prefix('movies')->group(function (){
        Route::get('/', 'index')->name('movies');
        Route::get('create', 'create')->name('movies.create');
        Route::post('store', 'store')->name('movies.store');
        Route::get('show/{id}', 'show')->name('movies.show');
        Route::get('edit/{id}', 'edit')->name('movies.edit');
        Route::put('edit/{id}', 'update')->name('movies.update');
        Route::delete('destroy/{id}', 'destroy')->name('movies.destroy');
    });
    



   
});